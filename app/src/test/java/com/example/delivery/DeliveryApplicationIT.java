package com.example.delivery;

import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.PortBinding;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.function.Consumer;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Basic integration test to verify that the app is properly configured.
 * Uses MySQL test container as data source.
 */
@Testcontainers
@SpringBootTest
@AutoConfigureMockMvc
public class DeliveryApplicationIT {

    private static final Consumer<CreateContainerCmd> PORT_BINDING =
            cmd -> cmd.withHostConfig(HostConfig.newHostConfig().withPortBindings(PortBinding.parse("3306:3306")));

    @Container
    private static final MySQLContainer<?> MY_SQL_CONTAINER = new MySQLContainer<>("mysql:5.7.33")
            .withDatabaseName("delivery")
            .withUsername("delivery")
            .withPassword("devonlydelivery")
            .withInitScript("restaurants.sql")
            .withCreateContainerCmdModifier(PORT_BINDING);

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenGettingHomePage_restaurantsAreListed() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(allOf(
                        containsString("Schnitzelfabrik"),
                        containsString("Trattoria Molto Bene"),
                        containsString("Nasi Padang"),
                        containsString("Szegediner")
                )));

    }
}
