USE delivery;

CREATE TABLE restaurants 
  (id int primary key, name varchar(255), style varchar(255), thumbnail_id varchar(255));

INSERT INTO restaurants VALUES 
  (1, 'Schnitzelfabrik','Austrian, German', '1001'),
  (2, 'Trattoria Molto Bene','Italian', '1002'),
  (3, 'Nasi Padang','Indonesian', '1003'),
  (4, 'Szegediner','Hungarian', '1004');

