package com.example.delivery.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Generating static file storage URLs for thumbnails.
 */
@Service
public class StaticStorage {

    private static final String THUMBNAIL_URL_PATTERN = "%s/%s.jpg";
    private final String staticStorageUrl;

    public StaticStorage(@Value("${delivery.static-storage}") String staticStorageUrl) {
        this.staticStorageUrl = staticStorageUrl;
    }

    public String getThumbnailUrlOf(RestaurantEntity entity) {
        return String.format(THUMBNAIL_URL_PATTERN, staticStorageUrl, entity.getThumbnailId());
    }

}
