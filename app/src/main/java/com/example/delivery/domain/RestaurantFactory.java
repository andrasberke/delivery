package com.example.delivery.domain;

import com.example.delivery.data.RestaurantEntity;
import com.example.delivery.data.StaticStorage;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Creating {@link Restaurant} objects from database entity.
 */
@Component
@AllArgsConstructor
public class RestaurantFactory {

    private final DeliveryTimeCalculator deliveryTimeCalculator;
    private final StaticStorage staticStorage;

    public Restaurant createRestaurant(RestaurantEntity entity) {
        return Restaurant.builder()
                .id(entity.getId())
                .name(entity.getName())
                .style(entity.getStyle())
                .thumbnailUrl(staticStorage.getThumbnailUrlOf(entity))
                .expectedDeliveryTime(deliveryTimeCalculator.getExpectedDeliveryTime(entity))
                .build();
    }
}
