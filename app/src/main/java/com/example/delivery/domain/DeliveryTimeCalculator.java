package com.example.delivery.domain;

import com.example.delivery.data.RestaurantEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Random;

/**
 * Service simulating the calculation of expected delivery times.<br/>
 *
 * This implementation returns random time estimates between 30 and 60 mins.
 * For demonstrational purposes only.
 */
@Component
@Slf4j
public class DeliveryTimeCalculator {

    private final static int MIN_DELIVERY_TIME = 30;
    private final static int MAX_DELIVERY_TIME = 60;
    private final static int ROUND = 5;

    private final Random random;
    private final long simulatedDelayMs;

    public DeliveryTimeCalculator(@Value("${delivery.perf.delay}") long simulatedDelayMs) {
        this.random = new Random();
        this.simulatedDelayMs = simulatedDelayMs;
    }

    public Duration getExpectedDeliveryTime(RestaurantEntity entity) {
        try {
            // Note that sleep was added for performance testing demo
            // It simulates cache / db access time which would be normally here in a real implementation
            Thread.sleep(simulatedDelayMs);
            return calculateDeliveryTime(entity);
        } catch (Exception e) {
            log.error("Exception while calculating delivery time", e);
            return Duration.ofMinutes(0);
        }

    }

    private Duration calculateDeliveryTime(RestaurantEntity entity) {
        // Random delivery time between MIN and MAX
        // For demo only
        int steps = (MAX_DELIVERY_TIME - MIN_DELIVERY_TIME) / ROUND;
        int variableDeliveryTime = random.nextInt(steps) * ROUND;
        return Duration.ofMinutes(MIN_DELIVERY_TIME + variableDeliveryTime);
    }
}
