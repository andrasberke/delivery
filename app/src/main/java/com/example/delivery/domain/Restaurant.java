package com.example.delivery.domain;

import lombok.Builder;
import lombok.Getter;

import java.time.Duration;

/**
 * Business entity of a restaurant.
 */
@Getter
@Builder
public class Restaurant {
    private final int id;
    private final String name;
    private final String style;
    private final String thumbnailUrl;
    private final Duration expectedDeliveryTime;
}
