package com.example.delivery.domain;

import com.example.delivery.data.RestaurantEntity;
import com.example.delivery.data.RestaurantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

/**
 * Service for managing restaurant entries.
 */
@Service
@AllArgsConstructor
public class RestaurantService {

    private final RestaurantRepository repository;
    private final RestaurantFactory factory;

    public List<Restaurant> getRestaurants() {
        Iterable<RestaurantEntity> data = repository.findAll();
        return stream(data.spliterator(), false)
                .map(factory::createRestaurant)
                .collect(toList());
    }

}
