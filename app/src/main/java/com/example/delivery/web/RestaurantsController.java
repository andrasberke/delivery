package com.example.delivery.web;

import com.example.delivery.domain.RestaurantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Web controller for displaying restaurant entries on the home page.
 */
@Controller
@Slf4j
@AllArgsConstructor
public class RestaurantsController {

    private final RestaurantService restaurantService;

    @GetMapping("/")
    public String homePage(Model model) {
        log.info("Hit '/'");
        model.addAttribute("restaurants", restaurantService.getRestaurants());
        return "home";
    }
}
