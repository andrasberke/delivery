# Performance testing with JMeter

## Aim

Test the deployed delivery application's behaviour under growing load.

## Prerequisites

- Java 11+
- JMeter with UltimateThreadGroup plugin installed (https://jmeter-plugins.org/wiki/UltimateThreadGroup/)

## Execution

The command specified in `run.sh` will execute a series of HTTP GET requests for the deployed application's home page
while simulating a gradually growing load.

See details in article https://randomlongitude.com/2021/05/29/handle-that-load/
