#!/bin/bash
set -e

if [[ -z "$1" ]]; then
  echo "image tag is not defined"
  exit 1
fi

if [[ -z "$AWS_ACCOUNT_ID" ]]; then
  echo "AWS_ACCOUNT_ID is not defined"
  exit 1
fi

# tag for ECR
repo=$AWS_ACCOUNT_ID.dkr.ecr.eu-central-1.amazonaws.com
image_name="$repo/delivery:$1"
echo "$image_name"
docker tag aberke/delivery:"$1" "$repo"/delivery:"$1"

# push to ECR
echo "ECR login"
aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin "$repo"
echo "Pushing image"
docker push "$image_name"
