#!/bin/bash
set -e

if [[ -z "$1" ]]; then
  echo "image tag is not defined"
  exit 1
fi

# build Docker image
echo "Build ..."
image_name="aberke/delivery:$1"
../app/gradlew bootBuildImage --imageName="$image_name" -p ../app/

echo "Built as $image_name"
