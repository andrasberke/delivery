#!/bin/bash
set -e

if [[ -z "$1" ]]; then
  echo "Deployment ID is not defined"
  exit 1
fi

status=$(aws deploy get-deployment \
  --deployment-id "$1" | jq '.deploymentInfo.status')

echo "Status of deployment $1 is $status"
