#!/bin/bash
set -e

if [[ -z "$1" ]]; then
  echo "ECS task revision not defined"
  exit 1
fi

if [[ -z "$AWS_ACCOUNT_ID" ]]; then
  echo "AWS_ACCOUNT_ID is not defined"
  exit 1
fi

tmp_dir=./tmp
mkdir -p $tmp_dir

# Remove output if exists
deployment_file="$tmp_dir/deployment-$1.json"
rm -f "$deployment_file"

# Define app spec content with new revision number
# revision is a placeholder in the template being substituted here with the new task revision
app_spec_content=$(REVISION=$1 envsubst < ./templates/code-deploy-appspec.json | jq 'tostring')
echo "AppSpecContent:"
echo "$app_spec_content"

# Create deployment descriptor containing the new AppSpecContent, write to output file
APP_SPEC_CONTENT=$app_spec_content envsubst < ./templates/code-deploy-deployment.json >> "$deployment_file"
echo "Deployment file:"
cat  "$deployment_file"

# Creating deployment, will trigger the rollout of the new version
echo "Create deployment"
aws deploy create-deployment \
  --cli-input-json file://"$deployment_file" \
  --region eu-central-1

echo "Deployment started, check status on web"
