#!/bin/bash
set -e

if [[ -z "$1" ]]; then
  echo "image tag is not defined"
  exit 1
fi

if [[ -z "$AWS_ACCOUNT_ID" ]]; then
  echo "AWS_ACCOUNT_ID is not defined"
  exit 1
fi

if [[ -z "$DELIVERY_MYSQL_HOST" ]]; then
  echo "DELIVERY_MYSQL_HOST is not defined"
  exit 1
fi

tmp_dir=./tmp
mkdir -p $tmp_dir

# Remove output if exists
task_definition_file="$tmp_dir/task-definition-$1.json"
rm -f "$task_definition_file"

# Create updated ECS task definition
# image name is a placeholder in the template being substituted here with the new image tag
image_name="$AWS_ACCOUNT_ID.dkr.ecr.eu-central-1.amazonaws.com/delivery:$1"
DELIVERY_IMAGE_NAME=$image_name envsubst < ./templates/ecs-task-definition.json >> "$task_definition_file"

# Registering new task revision based on the new image
# return value is the new revision number
echo "Registering task definition for $image_name"
revision=$(aws ecs register-task-definition \
  --cli-input-json file://"$task_definition_file" \
  --region eu-central-1 | jq '.taskDefinition.revision')

# Verify returned revision number
re='^[0-9]+$'
if ! [[ $revision =~ $re ]]; then
   echo "error: revision is not a number $revision"
   exit 1
fi

echo "Registered as revision $revision"
