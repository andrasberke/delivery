#!/bin/bash

set -e
version=5.7.33

echo "Starting mysql:$version"
container=$(docker run -d \
  -p 3306:3306 \
  -e MYSQL_RANDOM_ROOT_PASSWORD=yes \
  -e MYSQL_DATABASE=delivery \
  -e MYSQL_USER=delivery \
  -e MYSQL_PASSWORD=devonlydelivery \
  mysql:$version)

if [[ -z "$container" ]]; then
  echo "Container not started";
  exit 1;
fi

echo "$container"

echo "Wait for startup"
sleep 10s

echo "Exec init script"
init_script=$(<./restaurants.sql)
docker exec -it "$container" mysql -u delivery -pdevonlydelivery -e  "$init_script"

echo "Done."
