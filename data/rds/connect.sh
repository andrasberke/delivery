#!/bin/bash

if [[ -z "$1" ]]; then
  echo "RDS host not specified";
  exit 1;
fi

mysql -u admin -p -h "$1".eu-central-1.rds.amazonaws.com