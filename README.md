# Delivery

Simple Java Spring Boot project to demonstrate deployment to AWS Cloud. 

- Sample application hosted on Elastic Container Service (ECS), 
- Data stored in Relational Database Service (RDS), 
- Credentials accessed from Secrets Manager,
- Static resources hosted on Simple Storage Service (S3),
- Deployment done via CodeDeploy.

The sample application is a front page of food delivery service containing restaurant list and expected delivery times.

![Preview](preview.png)

## Repo structure

- `app` Website implemented using Spring Boot web, jpa, thymeleaf, connected to MySQL database containing restaurant info.
- `data` Contents of MySQL database both local and RDS versions. Static images (restaurant thumbnails) to be copied to S3.
- `deploy` Scripts to automate building and deploying container image of delivery app to ECS.
- `performance` JMeter test script for performance testing (used to demo autoscaling feature of ECS)

## Running locally

For development purposes the application can be connected to a locally running MySQL instance containing test data.

### Prerequisites

- JDK 11+
- Docker
- Bash

### 1. Init data

Navigate to the `/data/local` directory to init MySQL server container and insert test data into it using the following command:

`./mysql.sh`

### 2. Run the application

Run the following command from the `/app` directory:

`gradlew bootRun`

Open `http://localhost:8080` in your browser to verify that the site is up and running.

## Deployment on AWS

### Overview

The overview of the deployed service can be found below. The app is built as a container and hosted on AWS ECS Fargate, 
data is hosted on RDS, static files on S3.

![Preview](delivery-aws.png)
  
### Brief description of components

ECR: Elastic Container Registry, AWS private repository for Docker images, contains `delivery` image

ECS: Elastic Container Service, managed runtime to run the docker container(s) on Fargate

- task definition: "what to run", image(s) to pull from ECR, resource limits, env variables
- service: run task definition and maintain one instance of `delivery` running serverless in the ECS cluster
    
ECS Fargate: hosting the one or more instances of the delivery service in serverless mode

- private subnet, only accessible internally in the VPC
- to debug networking config or http responses, a publicly available host must be used
- security group accepting http traffic from ALB
- support for log aggregation, monitoring, auto-scaling, service discovery (e.g. `curl http://delivery-service.rndlng:8080/`)

ALB: application load balancer 

- public entry point with SSL termination and routing rules
- security group accepting HTTPS traffic from the public internet
- target group: load balancer target defining route to target instance(s)

CodeDeploy: automated deployment of services on AWS

- supports deployment of ECS tasks, blue-green deployment with ALB
- deployment history, rolling updates, rollback for failed deployments

RDS: fully managed database instance(s) running on AWS

- running MySQL engine
- hosting sample data
- automatic backups
- security group accepting traffic on 3306 from internal sources (in VPC)
- to connect via CLI `mysql -h $HOST -P 3306 -u admin -p`, only works inside VPC (public host or systems manager session is needed to connect from public internet)

S3: hosting static files

- contains thumbnails of food images
- read-only public access

Secrets Manager:

- stores sensitive information e.g. db credentials, auth tokens
- access managed by AWS policies
- built-in support for accessing MySQL user/password from Java

Route 53: DNS for resolving host name(s)

Certificate Manager: for managing SSL certificate(s), renewal

Cost Explorer: analyse associated costs

### Blue-green deployment with CodeDeploy

![Blue-green](blue-green.png)

CodeDeploy provides fully automated, zero-downtime rollout process for services running on ECS, 
based on blue-green deployment.

In this setup, the ALB is connected to two separate target groups and routing 100% of the traffic to the target group 1, 
running the live (green) version.

If a new version of the Docker image is pushed to ECR and the deployment process is initiated, CodeDeploy starts a new 
task in target group 2, running the new version (blue). If the new task is declared health, the ALB is instructed to
gradually route traffic to target group 2, hence the new version will be live. After a grace period the original green 
version is stopped and the rollout completed.

Beside zero-downtime, the main advantage of this setup is being able to rapidly abort or rollback releases. If the blue
version is misbehaving, the process is stopped and the green version will continue serving production. If an error is
detected late (after already stopped the green version), it still can be redeployed from the rollout history.

For demo purposes the Delivery application can be deployed using CodeDeploy with the process described above.

The deployment scripts can be found in the `/deploy` directory and may be launched from CLI (tested on Ubuntu 20.04.2)
It requires `aws` CLI configured with a user eligible for accessing the cluster resources. Running the steps on CI/CD 
server can be also done, though it is not configured at the moment.

Rollout steps:

- `01-build.sh` The Spring Boot application is built and packed into a Docker image, tagged with the provided version
- `02-push-to-ecr.sh` The built image is pushed to Elastic Container Registry (ECR)
- `03-update-task-definition.sh` A new revision of the ECS task definition is created, instructing ECS to run the service
using the new image tag
- `04-create-deployment.sh` Starts a CodeDeploy deployment for the service using the new task definition, 
rolls out the new image
- `05-check-deployment.sh` Check status of deployments created in step 4

## TODO
- CI/CD server integration
- build environment with CloudFormation
